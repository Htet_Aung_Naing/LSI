package com.lsi.thesis.android.lsi.Model;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ApplicationData extends RealmObject implements Serializable {
    int id;
    String app_name;
    RealmList<String> permissionlist;

    String description;
    RealmList<Double> vectorTransporseValue;

/*    String imageiconpath;
    byte[] imageicon;*/

/*    public byte[] getImageicon() {
        return imageicon;
    }

    public void setImageicon(byte[] imageicon) {
        this.imageicon = imageicon;
    }

    public String getImageiconpath() {
        return imageiconpath;
    }

    public void setImageiconpath(String imageiconpath) {
        this.imageiconpath = imageiconpath;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmList<String> getPermissionlist() {
        return permissionlist;
    }

    public void setPermissionlist(RealmList<String> permissionlist) {
        this.permissionlist = permissionlist;
    }

    public RealmList<Double> getVectorTransporseValue() {
        return vectorTransporseValue;
    }

    public void setVectorTransporseValue(RealmList<Double> vectorTransporseValue) {
        this.vectorTransporseValue = vectorTransporseValue;
    }

    public ApplicationData()
    {
        this.app_name = "";
        this.description = "";
        this.permissionlist = new RealmList<>();
       // this.imageiconpath = "";
        this.vectorTransporseValue = new RealmList<>();

    }
}
