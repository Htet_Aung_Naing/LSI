package com.lsi.thesis.android.lsi.Model;

import java.util.Comparator;


public class ComperatorClass implements Comparator<FinalSimilarityReslult>{

	@Override
	public int compare(FinalSimilarityReslult arg0, FinalSimilarityReslult arg1) {
		if (arg1.getSimilarityresult() < arg0.getSimilarityresult()) return -1;
        if (arg1.getSimilarityresult() > arg0.getSimilarityresult()) return 1;
        return 0;
	}

}
