package com.lsi.thesis.android.lsi.Adapter;

import android.content.Context;
import android.content.pm.PackageManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lsi.thesis.android.lsi.R;

import java.util.List;

public class Similarity_ResultList_Adapter extends BaseAdapter {

    Context context;
    String[] reslist;
    PackageManager packageManager;
    TextView txtResult;

    public Similarity_ResultList_Adapter(Context context, String[] mapplist)
    {
        this.context = context;
        packageManager = context.getPackageManager();
        this.reslist = mapplist;
    }

    @Override
    public int getCount() {

        return reslist.length;
    }

    @Override
    public Object getItem(int position) {
        return reslist[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        View view ;
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.cus_result_show, null);


       String result = reslist[position];
        if(result != null)
        {
            String[] splitresult = result.split(":");
            TextView txtname = view.findViewById(R.id.txtsimilarityresult);
            if(splitresult.length == 2)
                txtname.setText(position+1+". "+splitresult[0]+" ("+splitresult[1]+")");
            else if(splitresult.length == 3)
                txtname.setText(position+1+". "+splitresult[0]+" ("+splitresult[1]+")"+":"+splitresult[2]);
            else
                txtname.setText(position+1+". "+splitresult[0]);
        }

        return view;
    }

}
