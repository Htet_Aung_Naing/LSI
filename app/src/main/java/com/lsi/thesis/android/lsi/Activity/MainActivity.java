package com.lsi.thesis.android.lsi.Activity;

import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import com.lsi.thesis.android.lsi.Fragment.ApplicationCompareFragment;
import com.lsi.thesis.android.lsi.Fragment.ApplicationListSaveFragment;
import com.lsi.thesis.android.lsi.Fragment.MatrixMenuFragment;
import com.lsi.thesis.android.lsi.Fragment.PermissionListbyAppFragment;
import com.lsi.thesis.android.lsi.Fragment.ProfileFragment;
import com.lsi.thesis.android.lsi.Fragment.SavedApplicationListFragment;
import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.Model.PermissionsbyEachData;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.RealmMigration;

import io.realm.Realm;
import io.realm.RealmResults;


public class MainActivity extends AppCompatActivity {

    String hasdata = "";

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if(isconfirm)
            {
                RealmMigration.restore(this);
            }

        }
    }

    boolean isconfirm = false;
    //boolean hasData = false;
    private static final int REQUEST_WRITE_PERMISSION = 786;

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                isconfirm = true;
        }
        else
        {
            isconfirm = true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        hasdata = getIntent().getStringExtra("hasdata");

        if(hasdata == null)
            hasdata = "true";


       /* Realm.init(getApplicationContext());
        final Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
         RealmResults<ApplicationData> results = realm.where(ApplicationData.class).findAll();
         realm.commitTransaction();*/
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomTabBar = findViewById(R.id.navigationBar);


        if(hasdata.equals("false"))
        {
            requestPermission();
           //RealmMigration.restore(this);
        }
        bottomTabBar.setSelectedItemId(R.id.item_profile);
        Fragment selectedFragment =ProfileFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();



        bottomTabBar.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;

                        switch (item.getItemId()) {
                            case R.id.item_profile:
                                selectedFragment = ProfileFragment.newInstance();
                                break;
                          /*  case R.id.action_scan:
                                selectedFragment = PermissionListbyAppFragment.newInstance();
                                break;
                            case R.id.action_listview:
                                selectedFragment = ApplicationListSaveFragment.newInstance();
                                break;
                            case R.id.item_save:
                                selectedFragment = MatrixMenuFragment.newInstance();
                                break;
                            case R.id.action_app_list:
                                selectedFragment = SavedApplicationListFragment.newInstance(0);
                                break;*/
                            case R.id.action_compare:
                                selectedFragment = ApplicationCompareFragment.newInstance();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

    }
}
