package com.lsi.thesis.android.lsi.Fragment;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lsi.thesis.android.lsi.Dao.ApplicationDao;
import com.lsi.thesis.android.lsi.Model.PermissionsbyEachData;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.Utilization;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;


public class PermissionListbyAppFragment extends Fragment {



    private OnFragmentInteractionListener mListener;
    ArrayList<String> uniqueAllpermissionlist;
    List<ApplicationInfo> applicationlist;
    Context context;
    PackageManager packageManager;
    LinearLayout linearScanList;

    public PermissionListbyAppFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PermissionListbyAppFragment newInstance() {
        PermissionListbyAppFragment fragment = new PermissionListbyAppFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_permission_scan, container, false);
        linearScanList = view.findViewById(R.id.linearScanlist);
        context = this.getContext();
        ApplicationDao applicationDao = new ApplicationDao();
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        uniqueAllpermissionlist =applicationDao.getallPermissionList(realm);
        realm.commitTransaction();

        if(uniqueAllpermissionlist.size()>0)
        {
            context = this.getContext();
            packageManager = context.getPackageManager();
            applicationlist = Utilization.getAllApplicationList(context);
            for (ApplicationInfo app : applicationlist)
            {
                TextView txtHeader = new TextView(context);
                String[] permissions = Utilization.getRequestedPermissionList(context , app);
                PermissionsbyEachData scandata = new PermissionsbyEachData();
                txtHeader.setText(app.loadLabel(packageManager));
                txtHeader.setTypeface(txtHeader.getTypeface() , Typeface.BOLD);
                linearScanList.addView(txtHeader);
                if(permissions!=null)
                {
                    for (String permission : permissions)
                    {
                        if(uniqueAllpermissionlist.contains(permission))
                        {
                            TextView txtpermission = new TextView(context);
                            txtpermission.setText(permission);
                            linearScanList.addView(txtpermission);
                        }
                    }
                }
            }
        }

        return view;
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
