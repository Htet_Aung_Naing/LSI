package com.lsi.thesis.android.lsi.Fragment;

import android.app.Dialog;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.lsi.thesis.android.lsi.Adapter.Application_List_Adapter;
import com.lsi.thesis.android.lsi.Adapter.Application_RealmList_Adapter;
import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.CustomizeDialog;
import com.lsi.thesis.android.lsi.util.Utilization;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


public class SavedApplicationListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    Button btnDelete;
    // TODO: Customize parameters
    private int mColumnCount = 1;

    View appListView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SavedApplicationListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SavedApplicationListFragment newInstance(int columnCount) {
        SavedApplicationListFragment fragment = new SavedApplicationListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ListView lstView;
        appListView = inflater.inflate(R.layout.fragment_application_list, container, false);
        lstView = appListView.findViewById(R.id.lstview_application);
        btnDelete = appListView.findViewById(R.id.butDropDatabase);

        Realm.init(getContext());
        Realm realm = Realm.getDefaultInstance();
        realm.refresh();
        realm.beginTransaction();

        final RealmResults<ApplicationData> results = realm.where(ApplicationData.class).findAll();
        realm.commitTransaction();

        if(results.size()>0)
        {
            final RealmList<ApplicationData> realmList = new RealmList<>();
            realmList.addAll(results.subList(0, results.size()));

            lstView.setAdapter(new Application_RealmList_Adapter(getContext(),R.layout.cus_applist_view,realmList));

            lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String[]permissionlist ;
                    permissionlist = realmList.get(position).getPermissionlist().toArray(new String[0]);
                    if(permissionlist != null)
                    {
                        CustomizeDialog popup = new CustomizeDialog();
                        Dialog toast = popup.getpermissionListCusToast(getContext(),permissionlist);
                        toast.show();
                    }
                }
            });
        }

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Realm.init(getContext());
                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<ApplicationData> result = realm.where(ApplicationData.class).findAll();
                        result.deleteAllFromRealm();
                    }
                });
                Toast.makeText(getContext(),"Delete Data Successfully!" , Toast.LENGTH_LONG).show();
            }
        });



        return appListView;
    }
    
    public void combineAppListView(List<ResolveInfo> applist)
    {
        if(applist.size() > 0)
        {

        }
    }





}
