package com.lsi.thesis.android.lsi.Dao;

import android.content.Context;

import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.Model.MatrixA;
import com.lsi.thesis.android.lsi.Model.VectorValueMatrix;
import com.lsi.thesis.android.lsi.util.Utilization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Jama.Matrix;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class ApplicationDao {

    public List<ApplicationData> getallApp(Realm realm)
    {
        List<ApplicationData> reslist;


        RealmResults<ApplicationData> realmResults = realm.where(ApplicationData.class).findAll();
        reslist = realm.copyFromRealm(realmResults);

        return reslist;
    }

    private static void sortList(ArrayList<String> items){
        Collections.sort(items, String.CASE_INSENSITIVE_ORDER);
    }

    public ArrayList<String> getallPermissionList(Realm realm)
    {
        ArrayList<String> res = new ArrayList<>();
        List<ApplicationData> reslist;

        RealmResults<ApplicationData> realmResults = realm.where(ApplicationData.class).findAll();
        reslist = realm.copyFromRealm(realmResults);
        for (ApplicationData app: reslist)
        {
            for (String permission: app.getPermissionlist())
            {
                res.add(permission);
            }
        }
        res = Utilization.getUniqueData(res);
        sortList(res);
        return res;
    }

    public int getCountEachpermissionByApp(Context context , String permission , int id, Realm realm)
    {
        int res = 0;
        Realm.init(context);

        RealmResults<ApplicationData> realmResults = realm.where(ApplicationData.class).equalTo("id",id).findAll();
        if(realmResults != null)
        {
            if(realmResults.get(0).getPermissionlist().contains(permission))
                res = 1;
        }


        return res;
    }

    public boolean saveTrainedValue(ArrayList<VectorValueMatrix> vectormatrixlist , ArrayList<ApplicationData>applist, int threadshowvalue, Context context,Realm realm)
    {
        boolean flag = false;
        double[][] vmatrix = new double[vectormatrixlist.size()][vectormatrixlist.get(0).getValueList().getColumnData().size()];
        Matrix vectormatrix;

        for(int i=0 ; i<vectormatrixlist.size() ; i++)
        {
            for(int j=0 ; j<vectormatrixlist.get(i).getValueList().getColumnData().size() ; j++)
            {
                vmatrix[i][j] = Double.parseDouble(vectormatrixlist.get(i).getValueList().getColumnData().get(j));
            }
        }
        vectormatrix = new Matrix(vmatrix);
        vectormatrix = vectormatrix.transpose();

        for(int column = 0; column<vectormatrix.getColumnDimension(); column++)
        {
            RealmList<Double> threadshowlist = new RealmList<>();
            for(int row = 0; row<threadshowvalue ; row++)
            {
                threadshowlist.add(vectormatrix.get(row,column));
            }
            applist.get(column).setVectorTransporseValue(threadshowlist);
        }
        realm.insert(applist);
        flag = true;
        return flag;
    }

}
