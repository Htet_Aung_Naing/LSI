package com.lsi.thesis.android.lsi.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.lsi.thesis.android.lsi.Dao.ApplicationDao;
import com.lsi.thesis.android.lsi.Fragment.PermissionAppMatrixFragment;
import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.Model.DefaultPermissionList;
import com.lsi.thesis.android.lsi.Model.SingularValueMatrix;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.Services.SingularValueMatrixService;
import com.lsi.thesis.android.lsi.Services.UMatrixService;
import com.lsi.thesis.android.lsi.Services.VectorMatrixService;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Jama.Matrix;


;

public class SVUMatrixViewActivity extends AppCompatActivity {

    GridLayout gridPermissionMatrix;
    private PermissionAppMatrixFragment.OnFragmentInteractionListener mListener;
    PackageManager packageManager;
    String title;
    String menuid;
    DecimalFormat df = new DecimalFormat("#.00");
    Dialog exitDialog;


    Matrix resmatrix;
    Button butExportMatrix;

    TextView txtTitle;

    ScrollView scrollTable;

    HorizontalScrollView scrolview;
    Button butSaveMatrix;


    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.svu_matrix_view);
        txtTitle = findViewById(R.id.txt_title);
        title = getIntent().getStringExtra("title");
        menuid = getIntent().getStringExtra("menu");
        txtTitle.setText(title);
        butSaveMatrix = findViewById(R.id.but_save_matrix);
        butExportMatrix = findViewById(R.id.but_export_svumatrix);
        exitDialog = new Dialog(SVUMatrixViewActivity.this, R.style.FullHeightDialog);
        gridPermissionMatrix = new GridLayout(this);
        gridPermissionMatrix.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        gridPermissionMatrix.setOrientation(0);


        scrollTable = findViewById(R.id.scroll_SVUmatrix);
        scrollTable.setScrollbarFadingEnabled(false);

        scrolview = new HorizontalScrollView(this);

        scrollTable.addView(scrolview);
        packageManager = this.getPackageManager();

        if(menuid.equals("Singular"))
        {
            SingularValueMatrixService singularService = new SingularValueMatrixService();
            resmatrix = singularService.calculateSingularMatrix(getApplicationContext());
        }else if(menuid.equals("Eigen"))
        {
            VectorMatrixService vectorService = new VectorMatrixService();
            resmatrix = vectorService.calculateVectorMatrix(getApplicationContext());
        }else if(menuid.equals("umatrix"))
        {
            UMatrixService uMatrixService = new UMatrixService();
            resmatrix = uMatrixService.calculateUMatrix(getApplicationContext());
        }



      if(resmatrix != null) {
            gridPermissionMatrix.setColumnCount(resmatrix.getColumnDimension());
            gridPermissionMatrix.setRowCount(resmatrix.getRowDimension());
             /* TextView[] txtcompareList = new TextView[resmatrix.getColumnDimension()];

            for (int r = 0; r < resmatrix.getRowDimension(); r++) {

                for (int c = 0; c < resmatrix.getColumnDimension(); c++) {
                    txtcompareList[c]=new TextView(this);
                    txtcompareList[c].setTextSize(20);
                    txtcompareList[c].setPadding(30,0,30,0);
                    txtcompareList[c].setGravity(View.TEXT_ALIGNMENT_CENTER);
                    txtcompareList[c].setText(df.format(resmatrix.get(r, c)));
                    gridPermissionMatrix.addView(txtcompareList[c]);

                }

            }*/
            scrolview.addView(gridPermissionMatrix);
        }


        butSaveMatrix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(resmatrix!= null)
                {
                    if(menuid.equals("Singular"))
                    {
                        SingularValueMatrixService singularValueMatrixService = new SingularValueMatrixService();
                        if(singularValueMatrixService.saveMatrix(resmatrix.getArray(),getApplicationContext()))
                            Toast.makeText(getApplicationContext(),"Save Singular Matrix Successfully!", Toast.LENGTH_LONG).show();
                    }
                    else if(menuid.equals("Eigen"))
                    {
                        VectorMatrixService vectorMatrixService = new VectorMatrixService();
                        if(vectorMatrixService.saveMatrix(resmatrix.getArray(),getApplicationContext()))
                            Toast.makeText(getApplicationContext(),"Save Eigen Values Matrix Successfully!", Toast.LENGTH_LONG).show();
                    }
                    else if(menuid.equals("umatrix"))
                    {
                        UMatrixService uMatrixService = new UMatrixService();
                        if(uMatrixService.saveMatrix(resmatrix.getArray(),getApplicationContext()))
                            Toast.makeText(getApplicationContext(),"Save U Matrix Successfully!", Toast.LENGTH_LONG).show();
                    }
                }else
                {
                    exitDialog.setCancelable(true);
                    exitDialog.setCanceledOnTouchOutside(false);
                    exitDialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(Color.TRANSPARENT));
                    LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                    View layout = inflater.inflate(R.layout.exit_custom_dialog,
                            (ViewGroup) (findViewById(R.id.outerLayout)));
                    exitDialog.setContentView(layout);
                    Button butCancel;
                    TextView txtDialogMessage;

                    exitDialog.show();
                    butCancel = (Button) layout.findViewById(R.id.butCancel);
                    txtDialogMessage = layout.findViewById(R.id.txtConfirm);
                    txtDialogMessage.setText("The row count must be greater than the column count due to SVD method!");

                    butCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            exitDialog.cancel();
                        }
                    });
                }

            }
        });

        butExportMatrix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(resmatrix != null) {
                    if (menuid.equals("Singular")) {
                        SingularValueMatrixService singularservice = new SingularValueMatrixService();
                        try {
                            if (singularservice.exportMatrix(resmatrix.getArray()))
                                Toast.makeText(getApplicationContext(), "Export Singular Matrix at Download Folder!", Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else if (menuid.equals("Eigen")) {
                        VectorMatrixService vectorMatrixservice = new VectorMatrixService();
                        try {
                            if (vectorMatrixservice.exportMatrix(resmatrix.getArray()))
                                Toast.makeText(getApplicationContext(), "Export Vector Matrix at Download Folder!", Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else if (menuid.equals("umatrix")) {
                        UMatrixService uMatrixservice = new UMatrixService();
                        try {
                            if (uMatrixservice.exportMatrix(resmatrix.getArray()))
                                Toast.makeText(getApplicationContext(), "Export U Matrix at Download Folder!", Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else
                {
                    exitDialog.setCancelable(true);
                    exitDialog.setCanceledOnTouchOutside(false);
                    exitDialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(Color.TRANSPARENT));
                    LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                    View layout = inflater.inflate(R.layout.exit_custom_dialog,
                            (ViewGroup) (findViewById(R.id.outerLayout)));
                    exitDialog.setContentView(layout);
                    Button butCancel;
                    TextView txtDialogMessage;

                    exitDialog.show();
                    butCancel = (Button) layout.findViewById(R.id.butCancel);
                    txtDialogMessage = layout.findViewById(R.id.txtConfirm);
                    txtDialogMessage.setText("The row count must be greater than the column count due to SVD method!");

                    butCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            exitDialog.cancel();
                        }
                    });
                }
            }
        });


    }
}
