package com.lsi.thesis.android.lsi.Services;

import android.content.Context;
import android.os.Environment;

import com.lsi.thesis.android.lsi.Dao.ApplicationDao;
import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.Model.ColumnData;
import com.lsi.thesis.android.lsi.Model.SingularValueMatrix;
import com.lsi.thesis.android.lsi.Model.VectorValueMatrix;
import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class VectorMatrixService {
    DecimalFormat df = new DecimalFormat("#.0000");
    public  Matrix calculateVectorMatrix(Context con)
    {

        Context context = con;
        ApplicationDao appDao = new ApplicationDao();
        ArrayList<String> permissionList = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        Matrix vectorMatrix = null;

           permissionList = appDao.getallPermissionList(realm);
           // permissionList = new ArrayList<>(Arrays.asList(DefaultPermissionList.defaultPermissionlist));
            final RealmResults<ApplicationData> results = realm.where(ApplicationData.class).findAll();

            double [][] matrixA = new double[permissionList.size()][results.size()];


            //Matrix A = Matrix.identity(5, 5);
            if(permissionList.size() != 0 )
            {
                for (int r=0; r < matrixA.length; r++) {

                    for (int c=0; c<matrixA[r].length; c++) {

                        int count = appDao.getCountEachpermissionByApp(context , permissionList.get(r) , results.get(c).getId(), realm);
                        matrixA[r][c] = count;
                    }
                }
                realm.close();

                Matrix A = new Matrix(matrixA);

                //A = A.transpose().times(A);
                SingularValueDecomposition S = A.svd();
                vectorMatrix = S.getV();
            }



        return vectorMatrix;


    }

    public boolean saveMatrix(double[][] matrixA, Context context)
    {
        boolean flag = false;
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(VectorValueMatrix.class);
        List<VectorValueMatrix> arrayList2D = new ArrayList<>();
        for (int i = 0; i < matrixA.length; i++) {
            ColumnData columnData = new ColumnData();
            RealmList<String> eachRecord = new RealmList<>();
            for (int j = 0; j < matrixA[i].length; j++)
            {
                eachRecord.add(String.valueOf(matrixA[i][j]));
            }
            columnData.setColumnData(eachRecord);
            VectorValueMatrix matrixArealm = new VectorValueMatrix();
            matrixArealm.setValueList(columnData);
            arrayList2D.add(matrixArealm);
        }

        realm.insert(arrayList2D);
        realm.commitTransaction();
        realm.close();
        flag = true;
        return flag;
    }

    public boolean exportMatrix(double[][] matrixA) throws IOException {
        boolean flag = false;

        String csv = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"VectorMatrix.txt";
        CSVWriter writer = new CSVWriter(new FileWriter(csv));

        List<String[]> data = new ArrayList<String[]>();
        String[] title = new String[2];
        title[0] = "Total row:"+matrixA.length;
        title[1] = "Total column:"+matrixA[1].length;
        data.add(title);

        for (int i = 0; i < matrixA.length; i++) {
            String[] row = new String[matrixA[i].length];
            for (int j = 0; j < matrixA[i].length; j++)
            {
                row[j] = String.valueOf(df.format(matrixA[i][j]));
            }
            data.add(row);
        }

        writer.writeAll(data);

        writer.close();
        flag = true;
        return flag;
    }

}
