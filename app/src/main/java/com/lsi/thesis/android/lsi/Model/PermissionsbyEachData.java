package com.lsi.thesis.android.lsi.Model;

import java.util.ArrayList;

public class PermissionsbyEachData {

    String appname;
    ArrayList<String> permissionlist;

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public ArrayList<String> getPermissionlist() {
        return permissionlist;
    }

    public void setPermissionlist(ArrayList<String> permissionlist) {
        this.permissionlist = permissionlist;
    }

    public PermissionsbyEachData()
    {
        this.appname = "";
        this.permissionlist = new ArrayList<>();
    }
}
