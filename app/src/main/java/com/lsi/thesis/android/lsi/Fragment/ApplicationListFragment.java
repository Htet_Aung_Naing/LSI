package com.lsi.thesis.android.lsi.Fragment;

import android.app.Dialog;
import android.content.pm.ApplicationInfo;

import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lsi.thesis.android.lsi.Adapter.Application_List_Adapter;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.CustomizeDialog;
import com.lsi.thesis.android.lsi.util.Utilization;


import java.util.List;


public class ApplicationListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    View appListView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ApplicationListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ApplicationListFragment newInstance(int columnCount) {
        ApplicationListFragment fragment = new ApplicationListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ListView lstView;
        appListView = inflater.inflate(R.layout.fragment_application_list, container, false);
        lstView = appListView.findViewById(R.id.lstview_application);
        final List<ApplicationInfo> applist = Utilization.getAllApplicationList(getContext());
        lstView.setAdapter(new Application_List_Adapter(getContext(),R.layout.cus_applist_view,applist));

        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[]permissionlist ;
                permissionlist = Utilization.getRequestedPermissionList(getContext(),applist.get(position));
                if(permissionlist != null)
                {
                    CustomizeDialog popup = new CustomizeDialog();
                    Dialog toast = popup.getpermissionListCusToast(getContext(),permissionlist);
                    toast.show();
                }
            }
        });

        return appListView;
    }
    
    public void combineAppListView(List<ResolveInfo> applist)
    {
        if(applist.size() > 0)
        {

        }
    }





}
