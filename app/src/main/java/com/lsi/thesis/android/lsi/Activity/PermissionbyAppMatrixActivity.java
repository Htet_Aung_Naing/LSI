package com.lsi.thesis.android.lsi.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import android.widget.TextView;
import android.widget.Toast;

import com.lsi.thesis.android.lsi.Dao.ApplicationDao;
import com.lsi.thesis.android.lsi.Fragment.PermissionAppMatrixFragment;
import com.lsi.thesis.android.lsi.Model.ApplicationData;

import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.Services.MatrixAServices;




import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class PermissionbyAppMatrixActivity extends AppCompatActivity {

    GridLayout gridPermissionMatrix;
    List<ApplicationData> applicationlist;
    private PermissionAppMatrixFragment.OnFragmentInteractionListener mListener;
    PackageManager packageManager;

    Button butSaveMatrixA;
    ArrayList<String> uniqueAllpermissionlist;
    LinearLayout linearProgress;
    ProgressBar progressLoading;
    Dialog exitDialog;
    ScrollView scrollTable;
    int headercolumnCount = 0 ;
    int permissionrowCount = 0;
    HorizontalScrollView scrolview;
    Button butExportMatrix;
    int [][] permissionbyapplist;

    public void setHeader(GridLayout grid , ArrayList<ApplicationData> headerlist , Context context)
    {

        TextView blankHeader = new TextView(context);

        grid.addView(blankHeader);
        for (ApplicationData header :
             headerlist) {
            TextView txtheader = new TextView(context);
            txtheader.setTypeface(txtheader.getTypeface(), Typeface.BOLD);
            txtheader.setText(header.getApp_name());

            grid.addView(txtheader);
        }
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);

    }

    public ArrayList<String> getallPermissionList(List<ApplicationData> applist)
    {
        ArrayList<String> res = new ArrayList<>();
        for (ApplicationData app : applist)
        {
            for (String permission : app.getPermissionlist())
            {
                res.add(permission);
            }
        }
        return res;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.permission_matrix_view);
        gridPermissionMatrix = new GridLayout(this);
        exitDialog = new Dialog(PermissionbyAppMatrixActivity.this, R.style.FullHeightDialog);

        butSaveMatrixA = findViewById(R.id.but_save_matrix_A);
        gridPermissionMatrix.setOrientation(0);
        scrollTable = findViewById(R.id.scroll_matrixtable);
        scrollTable.setScrollbarFadingEnabled(false);
        butExportMatrix = findViewById(R.id.but_export_matrix_A);
        scrolview = new HorizontalScrollView(this);


        scrollTable.addView(scrolview);
        packageManager = this.getPackageManager();
        ApplicationDao applicationDao = new ApplicationDao();
        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        applicationlist = applicationDao.getallApp(realm);
        uniqueAllpermissionlist =applicationDao.getallPermissionList(realm);
        gridPermissionMatrix.setColumnCount(applicationlist.size()+1);
        gridPermissionMatrix.setRowCount(uniqueAllpermissionlist.size()+1);
        realm.commitTransaction();
        realm.close();
        setHeader(gridPermissionMatrix , (ArrayList<ApplicationData>) applicationlist, this);

        permissionbyapplist = new int[uniqueAllpermissionlist.size()][applicationlist.size()];
        for ( int p=0; p<uniqueAllpermissionlist.size(); p++ )
        {

            TextView txtpermission = new TextView(this);
            txtpermission.setText(uniqueAllpermissionlist.get(p));
            txtpermission.setTypeface(txtpermission.getTypeface(), Typeface.BOLD);
            gridPermissionMatrix.addView(txtpermission);
            TextView[] txtcompareList = new TextView[applicationlist.size()];

            for (int j=0; j<applicationlist.size(); j++)
            {
                RealmList<String> requestedPermissionList = applicationlist.get(j).getPermissionlist();
                boolean flag = false;
                if(requestedPermissionList.size() > 0)
                {
                    for ( int i=0 ; i<requestedPermissionList.size() ; i++)
                    {

                       // txtcompareList[j]=new TextView(this);

                        if(uniqueAllpermissionlist.get(p).equals(requestedPermissionList.get(i)))
                        {
                           flag = true;
                           break;
                        }
                    }


                    if(flag)
                    {
                        //txtcompareList[j].setText("1");
                        permissionbyapplist[p][j] = 1;
                    }
                    else
                    {
                       // txtcompareList[j].setText("0");
                        permissionbyapplist[p][j] = 0;
                    }


                   // gridPermissionMatrix.addView(txtcompareList[j]);
                }else
                {
                    TextView txtcompareCount = new TextView(this);
                    txtcompareCount.setText("0");
                   // gridPermissionMatrix.addView(txtcompareCount);
                }
            }

        }
        scrolview.addView(gridPermissionMatrix);


        butSaveMatrixA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(permissionbyapplist.length > 0)
                {
                    if(permissionbyapplist.length>permissionbyapplist[0].length)
                    {
                        Context context = getApplicationContext();
                        MatrixAServices matrixAServices = new MatrixAServices();
                        if(matrixAServices.saveMatrix(permissionbyapplist , context))
                            Toast.makeText(context,"Save Successfully", Toast.LENGTH_LONG).show();
                    }else
                    {
                        exitDialog.setCancelable(true);
                        exitDialog.setCanceledOnTouchOutside(false);
                        exitDialog.getWindow().setBackgroundDrawable(
                                new ColorDrawable(Color.TRANSPARENT));
                        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                        View layout = inflater.inflate(R.layout.exit_custom_dialog,
                                (ViewGroup) (findViewById(R.id.outerLayout)));
                        exitDialog.setContentView(layout);
                        Button butCancel;
                        TextView txtDialogMessage;

                        exitDialog.show();
                        butCancel = (Button) layout.findViewById(R.id.butCancel);
                        txtDialogMessage = layout.findViewById(R.id.txtConfirm);
                        txtDialogMessage.setText("The row count must be greater than the column count due to SVD method!");

                        butCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                              exitDialog.cancel();
                            }
                        });
                    }

                }else
                {
                    Toast.makeText(getApplicationContext(),"Can't Save!" , Toast.LENGTH_LONG).show();
                }
            }
        });

        butExportMatrix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(permissionbyapplist.length>0)
                {
                    MatrixAServices matrixAServices = new MatrixAServices();
                    try {
                        if(matrixAServices.exportMatrix(permissionbyapplist , uniqueAllpermissionlist))
                            Toast.makeText(getApplicationContext(),"Export CSV file to Download Folder!" , Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
