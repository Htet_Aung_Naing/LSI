package com.lsi.thesis.android.lsi.Services;

import android.content.Context;
import android.content.UriMatcher;
import android.os.Environment;
import android.util.Log;

import com.lsi.thesis.android.lsi.Dao.ApplicationDao;
import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.Model.ColumnData;
import com.lsi.thesis.android.lsi.Model.SingularValueMatrix;
import com.lsi.thesis.android.lsi.Model.UValueMatrix;
import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import Jama.Matrix;
import Jama.SingularValueDecomposition;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class UMatrixService {
    DecimalFormat df = new DecimalFormat("#.0000");
    public  Matrix calculateUMatrix(Context con)
    {

        Context context = con;
        ApplicationDao appDao = new ApplicationDao();
        ArrayList<String> permissionList = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        Matrix Utest = null;

        permissionList = appDao.getallPermissionList(realm);
           // permissionList = new ArrayList<>(Arrays.asList(DefaultPermissionList.defaultPermissionlist));
            final RealmResults<ApplicationData> results = realm.where(ApplicationData.class).findAll();

            if(results.size()< permissionList.size())
            {
                double [][] matrixA = new double[permissionList.size()][results.size()];


                //Matrix A = Matrix.identity(5, 5);
                if(permissionList.size() != 0 )
                {
                    for (int r=0; r < matrixA.length; r++) {

                        for (int c=0; c<matrixA[r].length; c++) {

                            int count = appDao.getCountEachpermissionByApp(context , permissionList.get(r) , results.get(c).getId(), realm);
                            matrixA[r][c] = count;
                        }
                    }
                }

                realm.close();

                Matrix originalmatrix = new Matrix(matrixA);
                Matrix A = new Matrix(matrixA);

                //A = A.transpose().times(A);
                SingularValueDecomposition S = A.svd();
          /*  Matrix multiply = S.getV().times(S.getS().inverse());
            Matrix sinverse = S.getS().inverse();
           Matrix uMatrix = originalmatrix.times(multiply);*/


                Utest = S.getU();
            }



        return Utest;


    }

    public double[][] changeMatrixForm(ArrayList<UValueMatrix> uvaluelist , int constCount)
    {
        int columncount = 0;
        if(constCount<uvaluelist.get(0).getValueList().getColumnData().size())//calculate const count from string.xml
            columncount = constCount;
        else
            columncount = uvaluelist.get(0).getValueList().getColumnData().size();

        double [][] res = new double[uvaluelist.size()][columncount];

        for(int row=0; row<uvaluelist.size(); row++)
        {
            for(int column=0; column<columncount; column++)
            {
                    res[row][column] = Double.parseDouble(uvaluelist.get(row).getValueList().getColumnData().get(column));
            }
        }

        return res;
    }

    public boolean saveMatrix(double[][] matrixA, Context context)
    {
        boolean flag = false;
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(UValueMatrix.class);
        RealmList<UValueMatrix> arrayList2D = new RealmList<UValueMatrix>();
        for (int i = 0; i < matrixA.length; i++) {
            ColumnData columnData = new ColumnData();
            RealmList<String> eachRecord = new RealmList<>();
            for (int j = 0; j < matrixA[i].length; j++)
            {
                eachRecord.add(String.valueOf(matrixA[i][j]));
            }
            columnData.setColumnData(eachRecord);
            UValueMatrix matrixArealm = new UValueMatrix();
            matrixArealm.setValueList(columnData);
            arrayList2D.add(matrixArealm);
        }

        realm.copyToRealm(arrayList2D);
        realm.commitTransaction();
        realm.close();
        flag = true;
        return flag;
    }

    public boolean exportMatrix(double[][] matrixA) throws IOException {
        boolean flag = false;

        String csv = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"UMatrix.txt";
        CSVWriter writer = new CSVWriter(new FileWriter(csv));

        List<String[]> data = new ArrayList<String[]>();
        String[] title = new String[2];
        title[0] = "Total row:"+matrixA.length;
        title[1] = "Total column:"+matrixA[1].length;
        data.add(title);

        for (int i = 0; i < matrixA.length; i++) {
            String[] row = new String[matrixA[i].length];
            for (int j = 0; j < matrixA[i].length; j++)
            {
                row[j] = String.valueOf(df.format(matrixA[i][j]));
            }
            data.add(row);
        }

        writer.writeAll(data);

        writer.close();
        flag = true;
        return flag;
    }

}
