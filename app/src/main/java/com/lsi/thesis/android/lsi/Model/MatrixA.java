package com.lsi.thesis.android.lsi.Model;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;

public class MatrixA extends RealmObject implements Serializable {

   ColumnData valuelist;


    public ColumnData getValuelist() {
        return valuelist;
    }

    public void setValuelist(ColumnData valuelist) {
        this.valuelist = valuelist;
    }

    public MatrixA()
    {
        this.valuelist = new ColumnData();
    }

}
