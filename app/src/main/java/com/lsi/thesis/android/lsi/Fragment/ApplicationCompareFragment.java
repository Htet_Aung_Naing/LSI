package com.lsi.thesis.android.lsi.Fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.lsi.thesis.android.lsi.Activity.CalculateSimilarityActivity;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.FilePath;
import com.lsi.thesis.android.lsi.util.Utilization;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;


import java.util.ArrayList;
import java.util.Arrays;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ApplicationCompareFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ApplicationCompareFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ApplicationCompareFragment extends Fragment {




    private OnFragmentInteractionListener mListener;
    ScrollView scrollpermission;
    Button butFileBrowse;
    View appCompareView;
    TextView txtAppname;
    String[] requestedPermission;
    boolean isconfirm ;
    private static final int REQUEST_WRITE_PERMISSION = 786;

    public ApplicationCompareFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ApplicationCompareFragment newInstance() {
        ApplicationCompareFragment fragment = new ApplicationCompareFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        appCompareView = inflater.inflate(R.layout.fragment_comparison_app, container, false);
        butFileBrowse = appCompareView.findViewById(R.id.but_file_choose);
        txtAppname = appCompareView.findViewById(R.id.txt_app_name);

        butFileBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermissionsAndOpenFilePicker();
            }
        });

        return appCompareView;
    }

    private void openFilePicker() {
        /*new MaterialFilePicker()
                .withActivity(getActivity())
                .withRequestCode(FILE_PICKER_REQUEST_CODE)
                .withHiddenFiles(true)
                .start();*/

        Intent intent = new Intent(getContext(), FilePickerActivity.class);
        //intent.putExtra(FilePickerActivity.ARG_FILE_FILTER, Pattern.compile(".*\\.txt$"));
        intent.putExtra(FilePickerActivity.ARG_CLOSEABLE, true);
        //intent.putExtra(FilePickerActivity.ARG_START_PATH, );
        //intent.putExtra(FilePickerActivity.ARG, true);

        startActivityForResult(intent, 1);

    }

    private void checkPermissionsAndOpenFilePicker() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            openFilePicker();
        }
    }

    private void showError() {
        Toast.makeText(getContext(), "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker();
                } else {
                    showError();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                    //String packagedata = data.getPackage();
                    Uri uri = data.getData();

                    PackageManager pm = getContext().getPackageManager();
                    String path2 = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);

                   // String fullPath1 = FilePath.getPath(getActivity(), uri);
                    PackageInfo info = pm.getPackageArchiveInfo(path2, 0);

                    if(info != null)
                    {
                        ApplicationInfo appinfo = info.applicationInfo;
                        String appname = pm.getApplicationLabel(appinfo).toString();
                        requestedPermission = Utilization.getRequestedPermissionList(getContext(),info.applicationInfo,path2);
                        Intent myIntent = new Intent(getContext(), CalculateSimilarityActivity.class);
                        myIntent.putExtra("app_name" , appname );
                        myIntent.putExtra("permission", new ArrayList<String>(Arrays.asList(requestedPermission)));
                        startActivity(myIntent);
                    }else
                    {
                        Toast.makeText(getContext(), "Invalid File Type!",
                                Toast.LENGTH_SHORT).show();
                    }

        }
    }

    private static final int FILE_SELECT_CODE = 0;
    public static final int PERMISSIONS_REQUEST_CODE = 0;
    public static final int FILE_PICKER_REQUEST_CODE = 1;

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
            Toast.makeText(getContext(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
