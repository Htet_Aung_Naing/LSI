package com.lsi.thesis.android.lsi.Services;

import android.content.Context;
import android.os.Environment;

import com.lsi.thesis.android.lsi.Model.ColumnData;
import com.lsi.thesis.android.lsi.Model.MatrixA;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class MatrixAServices {

    public boolean saveMatrix(int[][] matrixA, Context context)
    {
        boolean flag = false;
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmList<MatrixA> arrayList2D = new RealmList<MatrixA>();
        for (int i = 0; i < matrixA.length; i++) {
            ColumnData columnData = new ColumnData();
            RealmList<String> eachRecord = new RealmList<>();
            for (int j = 0; j < matrixA[i].length; j++)
            {
                eachRecord.add(String.valueOf(matrixA[i][j]));
            }
            columnData.setColumnData(eachRecord);
            MatrixA matrixArealm = new MatrixA();
            matrixArealm.setValuelist(columnData);
            arrayList2D.add(matrixArealm);
        }

        realm.commitTransaction();
        realm.close();
        flag = true;
        return flag;
    }


    public boolean exportMatrix(int[][] matrixA , ArrayList<String> uniquepermissions) throws IOException {
        boolean flag = false;

        String csv = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"matrixA.txt";
        CSVWriter writer = new CSVWriter(new FileWriter(csv));

        List<String[]> data = new ArrayList<String[]>();
        String[] title = new String[2];
        title[0] = "Total row:"+matrixA.length;
        title[1] = "Total column:"+matrixA[1].length;
        data.add(title);
        for (int i = 0; i < matrixA.length; i++) {
            StringBuilder sb = new StringBuilder();
            String[] row = new String[matrixA[i].length+1];
            row[0] = uniquepermissions.get(i);
            for (int j = 0; j < matrixA[i].length; j++)
            {
                row[j+1] = String.valueOf(matrixA[i][j]);
            }
            data.add(row);
        }

        writer.writeAll(data);

        writer.close();
        flag = true;
        return flag;
    }


}
