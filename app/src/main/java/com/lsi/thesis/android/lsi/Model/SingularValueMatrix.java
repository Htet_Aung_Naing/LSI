package com.lsi.thesis.android.lsi.Model;

import com.lsi.thesis.android.lsi.Services.SingularValueMatrixService;

import java.io.Serializable;

import io.realm.RealmObject;

public class SingularValueMatrix extends RealmObject implements Serializable{

    ColumnData valueList;

    public ColumnData getValueList() {
        return valueList;
    }

    public void setValueList(ColumnData valueList) {
        this.valueList = valueList;
    }

    public SingularValueMatrix()
    {
        this.valueList = new ColumnData();
    }

}
