package com.lsi.thesis.android.lsi.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Environment;

import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.Model.ComperatorClass;
import com.lsi.thesis.android.lsi.Model.DefaultPermissionList;
import com.lsi.thesis.android.lsi.Model.FinalSimilarityReslult;

import net.dongliu.apk.parser.ApkFile;
import net.dongliu.apk.parser.bean.ApkMeta;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Jama.Matrix;
import io.realm.RealmList;
import io.realm.RealmResults;

public class  Utilization {
    static DecimalFormat df = new DecimalFormat("#.0000");

    public static List<ApplicationInfo> getAllApplicationList(Context context)
    {
        List<ApplicationInfo> reslist = new ArrayList<>();
        PackageManager pm = context.getPackageManager();

        reslist = pm.getInstalledApplications(PackageManager.GET_META_DATA);


        return reslist;
    }

    public static List<String> calculateSamilarity(Matrix query, RealmResults<ApplicationData> applist)
    {
        List<String> res = new ArrayList<>();
        double totalnumerator = 0;
        double totalsquare = 0;
        double similarity = 0;
        double qsquare = 0;
        double tcosinesquareroot = 0;
        double tqcosinesquareroot = 0;
        String finalresult = "";
        ArrayList<FinalSimilarityReslult> rankinlist = new ArrayList<>();


        for(int j = 0; j<query.getColumnDimension(); j++)
        {
            qsquare  += Math.pow(query.get(0,j),2);
        }
        tqcosinesquareroot = Math.sqrt(qsquare);

        for (ApplicationData app:applist)
        {
            FinalSimilarityReslult rank = new FinalSimilarityReslult();
            totalnumerator = 0;
            totalsquare = 0;
            tcosinesquareroot = 0;
            for(int i=0; i<app.getVectorTransporseValue().size(); i++)
            {
                totalnumerator += Double.valueOf(df.format(query.get(0,i) * app.getVectorTransporseValue().get(i)));
              //  totalsquare += Double.valueOf(df.format(Math.pow(query.get(0,i),2) + Math.pow(app.getVectorTransporseValue().get(i),2)));
                totalsquare += Double.valueOf(df.format(Math.pow(app.getVectorTransporseValue().get(i),2)));

            }
            tcosinesquareroot = Math.sqrt(totalsquare);
            tcosinesquareroot = tcosinesquareroot * tqcosinesquareroot;
            totalsquare = totalsquare + qsquare;
            similarity = Double.valueOf(df.format(totalnumerator/(totalsquare-totalnumerator)));
            rank.setAppname(app.getApp_name());
            rank.setSimilarityresult(similarity);
            rank.setCosineresult(Double.valueOf(df.format(totalnumerator/tcosinesquareroot)));
            rankinlist.add(rank);
           // finalresult = similarity +":"+app.getApp_name();
            //res.add(finalresult);
        }
        Collections.sort(rankinlist , new ComperatorClass());
        res = getsortRank(rankinlist);
        return  res;
    }

    public static ArrayList<String> getsortRank(ArrayList<FinalSimilarityReslult> similarlist)
    {
        ArrayList<String> ranklist = new ArrayList<>();
        for (FinalSimilarityReslult res : similarlist)
        {
            String finalresult = res.getSimilarityresult() +":"+res.getAppname()+":"+res.getCosineresult();
            ranklist.add(finalresult);
        }
        return ranklist;
    }

    public static double[][] getValidPermissionListfromQuery(ArrayList<String> query,ArrayList<String> uniquepermissionlist)
    {
     //  List<String> plist = Arrays.asList(DefaultPermissionList.defaultPermissionlist);
        List<String> plist =uniquepermissionlist;
        double [][] res = new double[plist.size()][1];

        for (int i = 0; i< plist.size(); i++)
        {
            for(int j = 0; j<query.size(); j++)
            {
                String [] permissionspilit = query.get(j).split("\\.");
                if(permissionspilit[permissionspilit.length-1].equalsIgnoreCase(plist.get(i)))
                {
                    res[i][0] = 1;
                    break;
                }
                else res[i][0] = 0;
            }

        }

        return res;
    }

    public static String loadJSONfile(File file) {
        String json = null;
        try {
            InputStream is = new FileInputStream(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static boolean hasPermissions(File file) throws IOException, JSONException
    {
        boolean flag = false;
        RealmList<String> res = new RealmList<>();

        String appname = "";
        JSONObject obj = new JSONObject(loadJSONfile(file));
        JSONArray permissionArray = obj.getJSONArray("permissions");
        if(permissionArray != null)
        {
            if(permissionArray.length()>0)
                flag = true;
            else flag = false;
        }else flag = false;
        return flag;

    }

    public static void readpermissionfromJson(File file , ApplicationData app) throws IOException, JSONException
    {
        RealmList<String> res = new RealmList<>();

            String appname = "";
            JSONObject obj = new JSONObject(loadJSONfile(file));
            JSONArray permissionArray = obj.getJSONArray("permissions");
            if(obj.has("app_name"))
            {
                appname = obj.getString("app_name");
                app.setApp_name(appname);
            }

            if(permissionArray != null)
            {
               for(int i = 0; i<permissionArray.length(); i++)
               {
                   String [] permissionspilit = permissionArray.getString(i).split("\\.");
                  List<String> plist = Arrays.asList(DefaultPermissionList.defaultPermissionlist);

                  if(!plist.contains(permissionspilit[permissionspilit.length-1]))
                       res.add(permissionspilit[permissionspilit.length-1]);

                // res.add(permissionspilit[permissionspilit.length-1]);
               }
               app.setPermissionlist(res);
            }

    }

    public static String [] getRequestedPermissionList(Context context,ApplicationInfo applicationInfo,String apk)
    {

        List<String> permission = new ArrayList<>();
        try (ApkFile apkFile = new ApkFile(new File(apk))) {
            ApkMeta apkMeta = apkFile.getApkMeta();
            permission = apkMeta.getUsesPermissions();


        } catch (IOException e) {
            e.printStackTrace();
        }


        return permission.toArray(new String[0]);

    }


    public static String [] getRequestedPermissionList(Context context,ApplicationInfo applicationInfo)
    {

        PackageManager pm = context.getPackageManager();
        ArrayList<String> readablepermissions = new ArrayList<>();
        String[] permissionlist = null;
        PackageInfo packageInfo = new PackageInfo();
        try {

            packageInfo = pm.getPackageInfo(applicationInfo.packageName, PackageManager.GET_PERMISSIONS);
            permissionlist = packageInfo.requestedPermissions;
            if(permissionlist != null)
            {
                for (String permission :
                     permissionlist) {
                    PermissionInfo permissionInfos = pm.getPermissionInfo(permission, 0);
                    CharSequence per = permissionInfos.loadLabel(pm);
                    readablepermissions.add(per.toString());
                }

                permissionlist = readablepermissions.toArray(new String[0]);

            }



        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return permissionlist;
    }

    public static String [] getRealmPermissionList(Context context,ApplicationData applicationInfo)
    {
        PackageManager pm = context.getPackageManager();
        ArrayList<String> readablepermissions = new ArrayList<>();
        String[] permissionlist = null;
        PackageInfo packageInfo = new PackageInfo();
        try {

            permissionlist = applicationInfo.getPermissionlist().toArray(new String[0]);
            PermissionInfo permissionInfo;
            PermissionInfo[] perlist= packageInfo.permissions;
            if(permissionlist != null)
            {
                for (String permission :
                        permissionlist) {
                    PermissionInfo permissionInfos = pm.getPermissionInfo(permission, 0);
                    CharSequence per = permissionInfos.loadLabel(pm);
                    readablepermissions.add(per.toString());
                }

                permissionlist = readablepermissions.toArray(new String[0]);
            }



        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return permissionlist;
    }

    public static RealmList<String> getRequestedPermissionRealmList(Context context, ApplicationInfo applicationInfo)
    {
        PackageManager pm = context.getPackageManager();
        RealmList<String> readablepermissions = new RealmList<>();
        String[] permissionlist = null;
        PackageInfo packageInfo = new PackageInfo();
        try {

            packageInfo = pm.getPackageInfo(applicationInfo.packageName, PackageManager.GET_PERMISSIONS);
            permissionlist = packageInfo.requestedPermissions;
            PermissionInfo permissionInfo;
            PermissionInfo[] perlist= packageInfo.permissions;
            if(permissionlist != null)
            {
                for (String permission :
                        permissionlist) {
                    PermissionInfo permissionInfos = pm.getPermissionInfo(permission, 0);
                    CharSequence per = permissionInfos.loadLabel(pm);
                    readablepermissions.add(per.toString());
                }
            }



        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return readablepermissions;
    }

    public static ArrayList<String> getUniqueData(ArrayList<String>wordList)
    {
        ArrayList<String> result = new ArrayList<String>();
        Set<String> hs = new HashSet<>();
        hs.addAll(wordList);
        result.addAll(hs);
        return result;
    }

    public static ArrayList<String> getAllofAppPermissionList(Context context)
    {
        String[] res;
        ArrayList<String>reslist = new ArrayList<>();
        List<ApplicationInfo> applist = getAllApplicationList(context);
        for (ApplicationInfo appinfo : applist )
        {
            String[] permissionlist = getRequestedPermissionList(context , appinfo);
            if(permissionlist != null)
            {
                for ( String permission: permissionlist )
                {
                    reslist.add(permission);
                }
            }

        }
        if(reslist.size() > 0)
        {
           reslist = getUniqueData(reslist);

//            Set<String> set = new HashSet<String>(reslist);
//            res = new String[set.size()];

        }

        return reslist;
    }



    public static ArrayList<File> getFileListfromDirectory()
    {
        ArrayList<File> resList = new ArrayList<>();
        String directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();

            File file = new File(directory);

                File[] files = file.listFiles();
                if(files != null)
                {
                    for(int i = 0 ; i<files.length ; i++)
                    {
                        if(FilenameUtils.getExtension(files[i].getAbsolutePath()).equals("json"))
                            resList.add(files[i]);
                    }
                }
        return resList;
    }

}
