package com.lsi.thesis.android.lsi.util;


import android.content.Context;
import android.os.Environment;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import io.realm.Realm;

public   class RealmMigration {

    private static File EXPORT_REALM_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    private static String EXPORT_REALM_FILE_NAME = "lsi_default.realm";
    private static String IMPORT_REALM_FILE_NAME = "default.realm"; //
    static String asset_path = "file:///android_asset/";
    static Context context;
    Realm realm;

    private static File createFileFromInputStream(InputStream inputStream) {

        try{
            File f = new File(context.getCacheDir()+"/temp.realm");
            OutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while((length=inputStream.read(buffer)) > 0) {
                outputStream.write(buffer,0,length);
            }

            outputStream.close();
            inputStream.close();

            return f;
        }catch (IOException e) {
           Log.v(""," ");
        }

        return null;
    }

    private static String copyBundledRealmFile(String oldFilePath, String outFileName, Context context) {
        try {
            File file = new File(context.getFilesDir(), outFileName);

            FileOutputStream outputStream = new FileOutputStream(file);

            FileInputStream inputStream = new FileInputStream(createFileFromInputStream(context.getAssets().open("lsi_default.realm")));

            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();
            return file.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void restore(Context contex) {
        context = contex;
        String restoreFilePath = asset_path + "/" + "lsi_default.realm";
        copyBundledRealmFile(restoreFilePath, IMPORT_REALM_FILE_NAME,context);
        Log.d("status", "Data restore is done");
    }

    private String dbPath(){
        return realm.getPath();
    }

}
