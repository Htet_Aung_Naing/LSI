package com.lsi.thesis.android.lsi.Fragment;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.Utilization;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ApplicationListSaveFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ApplicationListSaveFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ApplicationListSaveFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    boolean isconfirm = false;

    Button butSaveApplist;

    public ApplicationListSaveFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ApplicationListSaveFragment newInstance() {
        ApplicationListSaveFragment fragment = new ApplicationListSaveFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View applistSaveView;
        applistSaveView = inflater.inflate(R.layout.activity_application_save, container, false);
        //requestPermission();
        isconfirm =true;
        butSaveApplist = applistSaveView.findViewById(R.id.butSaveApplication);

        butSaveApplist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(isconfirm)
                {
                    ArrayList<File> filelist = Utilization.getFileListfromDirectory();
                    try {
                        saveApplicationList(filelist);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getContext(),"Save Successfully!",Toast.LENGTH_LONG).show();
                }

            }


        });

        return applistSaveView;
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
        else
            {
                isconfirm = true;
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
           isconfirm = true;
        }
    }

    public void saveApplicationList(ArrayList<File> filelist) throws IOException, JSONException {

        Realm.init(getContext());
        Realm realm = Realm.getDefaultInstance();

         for (File file: filelist)
         {
             if(Utilization.hasPermissions(file))
             {
                 realm.beginTransaction();
                 ApplicationData application = realm.createObject(ApplicationData.class);
                 Number currentIdNum = realm.where(ApplicationData.class).max("id");
                 int nextId;
                 if(currentIdNum == null) {
                     nextId = 1;
                 } else {
                     nextId = currentIdNum.intValue() + 1;
                 }
                 application.setId(nextId);
                 Utilization.readpermissionfromJson(file,application);
                 realm.commitTransaction();
             }

         }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
