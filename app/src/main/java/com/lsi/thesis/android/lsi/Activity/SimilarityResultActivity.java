package com.lsi.thesis.android.lsi.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.ListView;
import android.widget.TextView;

import com.lsi.thesis.android.lsi.Adapter.Similarity_ResultList_Adapter;
import com.lsi.thesis.android.lsi.R;

import java.util.Arrays;


public class SimilarityResultActivity extends AppCompatActivity {

    Button butBack;
    ListView lstViewResult;
    TextView txtmalware_category;
    String[] resultlist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_similarity_result);
        butBack = findViewById(R.id.butBack);
        txtmalware_category = findViewById(R.id.txt_malware_category);
        lstViewResult = findViewById(R.id.resultlistview);
        resultlist = getIntent().getStringArrayExtra("resultlist");
        if(resultlist.length>0)
        {
            String[]split = resultlist[0].split(":");
            if(Double.parseDouble(split[0])>=0.8000)
                txtmalware_category.setText("App has high risk permissions!");
            else if(Double.parseDouble(split[0])>=0.5847)
                txtmalware_category.setText("App has medium risk permissions!");
            else
                txtmalware_category.setText("App has low risk permissions!");
        }

        lstViewResult.setAdapter(new Similarity_ResultList_Adapter(getApplicationContext(), resultlist));


        butBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeinIntent = new Intent(getApplicationContext() , MainActivity.class);
                startActivity(homeinIntent);
            }
        });
    }


}
