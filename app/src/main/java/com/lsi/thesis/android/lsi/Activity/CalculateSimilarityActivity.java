package com.lsi.thesis.android.lsi.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.lsi.thesis.android.lsi.Dao.ApplicationDao;
import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.Model.SingularValueMatrix;
import com.lsi.thesis.android.lsi.Model.UValueMatrix;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.Utilization;

import java.util.ArrayList;
import java.util.List;

import Jama.Matrix;
import io.realm.Realm;
import io.realm.RealmResults;

public class CalculateSimilarityActivity extends AppCompatActivity {

    TextView txtAppname;
    Button butCalculesimilarity;
    LinearLayout permissionLayout;
    String appname;
    ArrayList<String> permissionlist;
    LinearLayout layoutmatrix;
    ProgressBar pbarSimilarity;
    int constCount = 0;
    Dialog exitDialog;

    public void showProgress(boolean flag)
    {
        pbarSimilarity.setVisibility(flag ? View.VISIBLE : View.GONE);
        layoutmatrix.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    public Matrix splitMatrixrcByconstValue(Matrix q ,Matrix S, Matrix U)
    {

        U = U.getMatrix(0, U.getRowDimension(), 0 , constCount);
        S = S.getMatrix(0, constCount , 0 , constCount);
        S = S.inverse();
        q = q.times(S).times(U);
        return q;
    }

    public double[][] changeUMatrixForm(RealmResults<UValueMatrix> uvaluelist , int constCount)
    {
        int columncount = 0;
        if(constCount<uvaluelist.get(0).getValueList().getColumnData().size())//calculate const count from string.xml
            columncount = constCount;
        else
            columncount = uvaluelist.get(0).getValueList().getColumnData().size();

        double [][] res = new double[uvaluelist.size()][columncount];

        for(int row=0; row<uvaluelist.size(); row++)
        {
            for(int column=0; column<columncount; column++)
            {
                res[row][column] = Double.parseDouble(uvaluelist.get(row).getValueList().getColumnData().get(column));
            }
        }

        return res;
    }

    public double[][] changeSMatrixForm(RealmResults<SingularValueMatrix> uvaluelist , int constCount)
    {
        int columncount = constCount;
//        if(constCount<uvaluelist.get(0).getValueList().getColumnData().size())//calculate const count from string.xml
//            columncount = constCount;
//        else
//            columncount = uvaluelist.get(0).getValueList().getColumnData().size();

        double [][] res = new double[constCount][columncount];

        for(int row=0; row<columncount; row++)
        {
            for(int column=0; column<columncount; column++)
            {
                res[row][column] = Double.parseDouble(uvaluelist.get(row).getValueList().getColumnData().get(column));
            }
        }

        return res;
    }

    public void appearDialog(String resmessage)
    {
        exitDialog.setCancelable(true);
        exitDialog.setCanceledOnTouchOutside(false);
        exitDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View layout = inflater.inflate(R.layout.exit_custom_dialog,
                (ViewGroup) (findViewById(R.id.outerLayout)));
        exitDialog.setContentView(layout);
        Button butCancel;
        TextView txtDialogMessage;

        exitDialog.show();
        butCancel = (Button) layout.findViewById(R.id.butCancel);
        txtDialogMessage = layout.findViewById(R.id.txtConfirm);
        txtDialogMessage.setText(resmessage);

        butCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitDialog.cancel();
            }
        });
    }



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculate_similarity);
        txtAppname = findViewById(R.id.txtAppnametitle);
        pbarSimilarity = findViewById(R.id.pgbarsimilarity);
        butCalculesimilarity = findViewById(R.id.but_calculate_similarity);
        permissionLayout = findViewById(R.id.permissionLayout);
        layoutmatrix = findViewById(R.id.layoutButSimilarity);
        exitDialog = new Dialog(CalculateSimilarityActivity.this, R.style.FullHeightDialog);
        appname = getIntent().getStringExtra("app_name");
        permissionlist = getIntent().getStringArrayListExtra("permission");
        txtAppname.setText(appname);
        constCount = Integer.parseInt(getString(R.string.threadshow_count));
        showProgress(false);
        for(String permission : permissionlist)
        {
            TextView txtpermission = new TextView(getApplicationContext());
            txtpermission.setText(permission);
            txtpermission.setTextSize(15);
            txtpermission.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            txtpermission.setGravity(View.TEXT_ALIGNMENT_CENTER);
            permissionLayout.addView(txtpermission);
        }

        butCalculesimilarity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                if(permissionlist.size()>0)
                {
                    Realm.init(getApplicationContext());
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    RealmResults<SingularValueMatrix> smatrix = realm.where(SingularValueMatrix.class).findAll();
                    RealmResults<ApplicationData> appResults = realm.where(ApplicationData.class).findAll();
                    RealmResults<UValueMatrix> umatrix = realm.where(UValueMatrix.class).findAll();

                    ArrayList<String> uniqueAllpermissionlist;
                    ApplicationDao applicationDao = new ApplicationDao();
                    uniqueAllpermissionlist =applicationDao.getallPermissionList(realm);

                    double[][] querymatrix = Utilization.getValidPermissionListfromQuery(permissionlist , uniqueAllpermissionlist);
                    if(querymatrix.length>0)
                    {
                        Matrix q = new Matrix(querymatrix);
                        Matrix U = null;
                        Matrix S = null;
                        //q = q.transpose();
                        if(umatrix.size()>0)
                            U = new Matrix(changeUMatrixForm(umatrix,constCount));
                        if(smatrix.size()>0)
                        {
                            S = new Matrix(changeSMatrixForm(smatrix,constCount));
                            S = S.inverse();
                        }

                        q = q.transpose().times(U);
                        q = q.times(S);
                        realm.commitTransaction();
                        String resmessage = "";


                        List<String> similaritylist  = Utilization.calculateSamilarity(q,appResults);
                        if(similaritylist.size()>0)
                        {
                            String[] result = similaritylist.toArray(new String[0]);
                            /*Bundle b=new Bundle();
                            b.putStringArray("resultlist", result);
                            Intent finalReslutintent = new Intent(getApplicationContext() , SimilarityResultActivity.class);
                            finalReslutintent.putExtras(b);
                            startActivity(finalReslutintent);*/
                            String[]split = result[0].split(":");
                            if(Double.parseDouble(split[0])>=0.8000)
                                resmessage = "App has high risk permissions!";
                            else if(Double.parseDouble(split[0])>=0.5847)
                                resmessage = "App has medium risk permissions!";
                            else
                                resmessage = "App has low risk permissions!";
                            appearDialog(resmessage);

                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"This application doesn't have permissions to detect!" , Toast.LENGTH_LONG).show();
                    }

                }else
                {
                    Toast.makeText(getApplicationContext(),"This application doesn't have permissions to detect!" , Toast.LENGTH_LONG).show();
                }
                showProgress(false);

            }
        });

    }
}