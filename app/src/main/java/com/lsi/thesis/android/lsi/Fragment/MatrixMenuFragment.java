package com.lsi.thesis.android.lsi.Fragment;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.lsi.thesis.android.lsi.Activity.PermissionbyAppMatrixActivity;
import com.lsi.thesis.android.lsi.Activity.SVUMatrixViewActivity;
import com.lsi.thesis.android.lsi.Dao.ApplicationDao;
import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.Model.ColumnData;
import com.lsi.thesis.android.lsi.Model.MatrixA;
import com.lsi.thesis.android.lsi.Model.VectorValueMatrix;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.Utilization;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


public class MatrixMenuFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // The request code must be 0 or greater.
    private static final int PLUS_ONE_REQUEST_CODE = 0;
    // The URL to +1.  Must be a valid URL.
    private final String PLUS_ONE_URL = "http://developer.android.com";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String[] matrixMenuList = {"Permission List by Each Application","Singular Values Matrix","Eigen Values Matrix","U Matrix"};
    ListView lstMatrixMenu;
    LinearLayout layoutprogress;
    ProgressBar pgbar ;
    Button butCleanData;
    Button butProcessData;
    Button butTrainDataSet;



    public MatrixMenuFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static MatrixMenuFragment newInstance() {
        MatrixMenuFragment fragment = new MatrixMenuFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        showProgress(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void showProgress(boolean flag)
    {
        layoutprogress.setVisibility(flag ? View.VISIBLE : View.GONE);
     lstMatrixMenu.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.show_matrix_list_view, container, false);
        pgbar = view.findViewById(R.id.pgbarMatrix);

        butCleanData = view.findViewById(R.id.but_clean_data);
        butProcessData = view.findViewById(R.id.but_process_data);
        lstMatrixMenu = view.findViewById(R.id.matrix_menu_list);
        lstMatrixMenu.setHeaderDividersEnabled(true);
        ArrayAdapter<String> menulistadapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, matrixMenuList);
        lstMatrixMenu.setAdapter(menulistadapter);
        layoutprogress = view.findViewById(R.id.layoutMatrix);
        showProgress(false);
        lstMatrixMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                showProgress(true);
                if(matrixMenuList[position].equals("Permission List by Each Application"))
                {
                    Intent myIntent = new Intent(getContext(), PermissionbyAppMatrixActivity.class);
                    startActivity(myIntent);

                }else if(matrixMenuList[position].equals("Singular Values Matrix"))
                {
                    Intent myIntent = new Intent(getContext(), SVUMatrixViewActivity.class);
                    myIntent.putExtra("title" , "Singular Values Matrix" );
                    myIntent.putExtra("menu", "Singular");
                    startActivity(myIntent);
                }
                else if(matrixMenuList[position].equals("Eigen Values Matrix"))
                {
                    Intent myIntent = new Intent(getContext(), SVUMatrixViewActivity.class);
                    myIntent.putExtra("title" , "Eigen Values Matrix" );
                    myIntent.putExtra("menu", "Eigen");
                    startActivity(myIntent);
                }
                else if(matrixMenuList[position].equals("U Matrix"))
                {
                    Intent myIntent = new Intent(getContext(), SVUMatrixViewActivity.class);
                    myIntent.putExtra("title" , "U Matrix" );
                    myIntent.putExtra("menu", "umatrix");
                    startActivity(myIntent);
                }

            }
        });

        butCleanData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Realm.init(getContext());
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.deleteAll();
                realm.commitTransaction();
                realm.close();
                Toast.makeText(getContext(),"Delete Successfully!",Toast.LENGTH_LONG).show();
            }
        });

        butProcessData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                Realm.init(getContext());
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                RealmResults<VectorValueMatrix> results = realm.where(VectorValueMatrix.class).findAll();
                RealmResults<ApplicationData> appResults = realm.where(ApplicationData.class).findAll();

                if(results.size()>0 && appResults.size()>0) {
                    ArrayList<VectorValueMatrix> vectormatrix = new ArrayList<>();
                    ArrayList<ApplicationData> realmApplist = new ArrayList<>();
                    vectormatrix.addAll(results);
                    realmApplist.addAll(appResults);
                    ApplicationDao applicationDao = new ApplicationDao();
                    if(applicationDao.saveTrainedValue(vectormatrix,realmApplist,Integer.parseInt(getResources().getString(R.string.threadshow_count)),getContext(),realm))
                    {
                        Toast.makeText(getContext(),"Process Successfully!",Toast.LENGTH_LONG).show();
                        showProgress(false);
                    }

                    else
                    {
                        showProgress(false);
                        Toast.makeText(getContext(),"Process Fail!",Toast.LENGTH_LONG).show();

                    }
                    realm.commitTransaction();
                    realm.close();

                }
                else {
                    realm.commitTransaction();
                    realm.close();
                    showProgress(false);
                    Toast.makeText(getContext(), "Can't Process right now!", Toast.LENGTH_LONG).show();
                }

            }
        });

       /* butTrainDataSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<File> filelist = Utilization.getFileListfromDirectory();
                try {
                    saveApplicationList(filelist);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getContext(),"Save Successfully!",Toast.LENGTH_LONG).show();
            }
        });*/

        return view;
    }


    public void saveApplicationList(ArrayList<File> filelist) throws IOException, JSONException {

        Realm.init(getContext());
        Realm realm = Realm.getDefaultInstance();

        for (File file: filelist)
        {
            if(Utilization.hasPermissions(file))
            {
                realm.beginTransaction();
                ApplicationData application = realm.createObject(ApplicationData.class);
                Number currentIdNum = realm.where(ApplicationData.class).max("id");
                int nextId;
                if(currentIdNum == null) {
                    nextId = 1;
                } else {
                    nextId = currentIdNum.intValue() + 1;
                }
                application.setId(nextId);
                Utilization.readpermissionfromJson(file,application);
                realm.commitTransaction();
            }

        }

    }




}
