package com.lsi.thesis.android.lsi.Fragment;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.lsi.thesis.android.lsi.Model.DefaultPermissionList;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.Utilization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PermissionAppMatrixFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class PermissionAppMatrixFragment extends Fragment {

    GridLayout gridPermissionMatrix;
    List<ApplicationInfo> applicationlist;
    private OnFragmentInteractionListener mListener;
    PackageManager packageManager;
    Context context;
    ArrayList<String> uniqueAllpermissionlist;

    public PermissionAppMatrixFragment() {
        // Required empty public constructor
    }

    public static PermissionAppMatrixFragment newInstance() {
        PermissionAppMatrixFragment fragment = new PermissionAppMatrixFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = this.getContext();
        packageManager = context.getPackageManager();
        View view = inflater.inflate(R.layout.permission_matrix_view, container, false);

        applicationlist = Utilization.getAllApplicationList(context);
        TableRow headerRow = new TableRow(getContext());
        TextView blankHeader = new TextView(getContext());
        headerRow.addView(blankHeader);
        for (ApplicationInfo appInfo: applicationlist)
        {
            TextView appNameView = new TextView(getContext());
            appNameView.setText(appInfo.loadLabel(packageManager));
            appNameView.setTypeface(appNameView.getTypeface() , Typeface.BOLD);
            headerRow.addView(appNameView);
        }
        uniqueAllpermissionlist = new ArrayList<>(Arrays.asList(DefaultPermissionList.defaultPermissionlist));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
