package com.lsi.thesis.android.lsi.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import com.lsi.thesis.android.lsi.R;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;




import java.util.ArrayList;

/**
 * Created by Htet Aung Naing on 10/27/2016.
 */

public class CustomizeDialog {

    public Dialog getpermissionListCusToast(Context context , String[]permissionlist)
    {
        Dialog permissinDialog = new Dialog(context);
        permissinDialog.setCancelable(true);
        permissinDialog.setCanceledOnTouchOutside(false);
        permissinDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        ListView lstPermissionView;
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout;
        ArrayAdapter<String> permissionadaper = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, android.R.id.text1, permissionlist);
        layout = inflater.inflate(R.layout.cus_permission_list, null);
        lstPermissionView = layout.findViewById(R.id.permission_list);
        lstPermissionView.setAdapter(permissionadaper);
        permissinDialog.setContentView(layout);

        return permissinDialog;
    }


}
