package com.lsi.thesis.android.lsi.Model;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ColumnData extends RealmObject implements Serializable{
    RealmList<String> columnData;

    public RealmList<String> getColumnData() {
        return columnData;
    }

    public void setColumnData(RealmList<String> columnData) {
        this.columnData = columnData;
    }

    public ColumnData()
    {
        this.columnData = new RealmList<>();
    }
}
