package com.lsi.thesis.android.lsi.Adapter;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lsi.thesis.android.lsi.R;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Application_List_Adapter extends ArrayAdapter<ApplicationInfo> {

    Context context;
    List<ApplicationInfo> appList;
    PackageManager packageManager;

    public Application_List_Adapter(@NonNull Context context, int resource, List<ApplicationInfo> mapplist)
    {
        super(context, resource,mapplist);
        this.context = context;
        packageManager = context.getPackageManager();
        this.appList = mapplist;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (null == view) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.cus_applist_view, null);
        }

        ApplicationInfo appinfo = appList.get(position);
        if(appinfo != null)
        {
            TextView txtname = view.findViewById(R.id.txt_app_name);
            txtname.setText(appinfo.loadLabel(packageManager));

        }

        return view;
    }

}
