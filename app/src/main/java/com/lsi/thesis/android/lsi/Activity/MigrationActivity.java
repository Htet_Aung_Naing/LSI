package com.lsi.thesis.android.lsi.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.LinearLayout;

import com.lsi.thesis.android.lsi.Model.ApplicationData;
import com.lsi.thesis.android.lsi.R;
import com.lsi.thesis.android.lsi.util.RealmMigration;

import io.realm.Realm;
import io.realm.RealmResults;

public class MigrationActivity extends AppCompatActivity {
    LinearLayout layoutprogress;
    private static final int REQUEST_WRITE_PERMISSION = 786;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_migration);
        layoutprogress = findViewById(R.id.layout_migration);
        showProgress(true);

        if(hasTrainedData())
        {
            Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
            myIntent.putExtra("hasdata", "true");
            startActivity(myIntent);

        }else
        {
            /*RealmMigration.restore(this);
            showProgress(false);
            Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
            myIntent.putExtra("hasdata", "true");
            startActivity(myIntent);*/
            requestPermission();
        }

    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                RealmMigration.restore(this);
                showProgress(false);
            Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
            myIntent.putExtra("hasdata", "true");
            startActivity(myIntent);
        }
    }

    public boolean hasTrainedData()
    {
        boolean flag = false;

        Realm.init(getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<ApplicationData> results = realm.where(ApplicationData.class).findAll();
        realm.commitTransaction();
        if(results.size()>0)
        {
            flag = true;
        }
        realm.close();

        return flag;
    }

    public void showProgress(boolean flag)
    {
        layoutprogress.setVisibility(flag ? View.VISIBLE : View.GONE);
    }

}
