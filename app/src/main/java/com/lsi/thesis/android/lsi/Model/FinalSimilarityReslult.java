package com.lsi.thesis.android.lsi.Model;

import android.support.annotation.NonNull;

import java.util.Comparator;

public class FinalSimilarityReslult {

    String appname;
    double similarityresult;
    double cosineresult;

    public double getCosineresult() {
        return cosineresult;
    }

    public void setCosineresult(double cosineresult) {
        this.cosineresult = cosineresult;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public double getSimilarityresult() {
        return similarityresult;
    }

    public void setSimilarityresult(double similarityresult) {
        this.similarityresult = similarityresult;
    }

    public FinalSimilarityReslult()
    {
        this.appname = "";
        this.similarityresult = 0;
        this.cosineresult = 0;
    }

    public static Comparator<FinalSimilarityReslult> DocumentSimilarityRank = new Comparator<FinalSimilarityReslult>()
    {

        public int compare(FinalSimilarityReslult s1, FinalSimilarityReslult s2) {

            double res1 = s1.getSimilarityresult();
            double res2 = s2.getSimilarityresult();

            return (int) (res2-res1);
        }
    };


}
