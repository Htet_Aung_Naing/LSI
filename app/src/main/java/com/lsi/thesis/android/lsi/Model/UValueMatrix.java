package com.lsi.thesis.android.lsi.Model;

import java.io.Serializable;

import io.realm.RealmObject;

public class UValueMatrix extends RealmObject implements Serializable{

    ColumnData valueList;

    public ColumnData getValueList() {
        return valueList;
    }

    public void setValueList(ColumnData valueList) {
        this.valueList = valueList;
    }

    public UValueMatrix()
    {
        this.valueList = new ColumnData();
    }

}
